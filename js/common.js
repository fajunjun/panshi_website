function morePlay(){
    $(".more").hover(
        function() {
            $(this)
                .stop()
                .animate({ backgroundColor: "#FFC107" });
        },
        function() {
            $(this)
                .stop()
                .animate({ backgroundColor: "#2E84D2" });
        }
    );    
}
function amap() {
    var map = new AMap.Map('amap', {
        zoom: 18,
        center: [108.96855, 34.258731]
    });
    map.plugin(["AMap.ToolBar"], function () {
        var toolBar = new AMap.ToolBar();
        map.addControl(toolBar);
    });
    var infoWindow = new AMap.InfoWindow({
        content: "<h5>陕西时代汇达教育科技有限公司</h5><p>地址：西安市碑林区东大街商联大厦638室</p>"
    });
    infoWindow.open(map, map.getCenter());
}